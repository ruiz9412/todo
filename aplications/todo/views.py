from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import TodoItem

def todoView(request):
  todoItems = TodoItem.objects.all()
  content = {
    'items': todoItems
  }
  return render(request, 'todo.html', content)

def addTodo(request):
  newTodo = TodoItem(content = request.POST['content'])
  newTodo.save()
  return HttpResponseRedirect('/todo/')

def deleteTodo(request, idTodo):
  getItem = TodoItem.objects.get(id = idTodo)
  getItem.delete()
  return HttpResponseRedirect('/todo/')
